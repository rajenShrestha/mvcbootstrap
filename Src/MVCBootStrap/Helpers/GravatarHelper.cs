﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 4/2/2015 10:37:28 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace WebAppMVCBootStrap.Helpers
{
    public static class GravatarHelper
    {
        public static HtmlString GravatarImage(this System.Web.Mvc.HtmlHelper htmlHelper, string emailAddress, GravatarOptions options = null)
        {
            if (options == null)
            {
                options = GravatarOptions.GetDefaults();
            }

            var imgTag = new TagBuilder("img");

            if (!string.IsNullOrEmpty(options.CssClass))
            {
                imgTag.AddCssClass(options.CssClass);
            }

            emailAddress = string.IsNullOrEmpty(emailAddress) ? string.Empty : emailAddress.Trim().ToLower();

            imgTag.Attributes.Add("src", 
                string.Format("http://www.gravatar.com/avatar/{0}?s={1}{2}{3}", GetMd5Hash(emailAddress), options.Size, "&d=" + options.DefaultImageType, "&r=" + options.RatingLevel));

            return new HtmlString(imgTag.ToString(TagRenderMode.SelfClosing));
        }

        private static string GetMd5Hash(string emailAddress)
        {
            byte[] data = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(emailAddress));
            var sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}