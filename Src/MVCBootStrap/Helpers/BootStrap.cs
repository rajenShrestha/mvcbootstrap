﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAppMVCBootStrap.Helpers
{
    public class BootStrap
    {
        public const string BundleBase = "~/Content/css/";

        public class Theme
        {
            public const string Cerulean = "cerulean";
            public const string Darky = "darky";
            public const string United = "united";
            public const string Yeti = "yeti";
        }

        public static HashSet<string> Themes = new HashSet<string>() {
            Theme.Cerulean,
            Theme.Darky,
            Theme.United,
            Theme.Yeti
        };

        public static string Bundles(string themename)
        {
            return BundleBase + themename;
        }
    }
}
