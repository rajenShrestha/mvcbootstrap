﻿(function() {
  var Person, collection, item, rajen, stewie, uppercased;

  Person = (function() {
    function Person(age, name) {
      this.age = age;
      this.name = name;
    }

    Person.prototype.can_drive = function() {
      var minimum_driving_age;
      minimum_driving_age = 17;
      return this.age >= minimum_driving_age;
    };

    return Person;

  })();

  rajen = new Person(20, "Rajen");

  stewie = new Persion(13, "Stewie");

  alert("" + rajen.name + " can drive " + (rajen.can_drive()) + " " + stewie + " can drive " + (stewie.can_drive()));

  collection = {
    "one": "one",
    "two": "two",
    "three": "three"
  };

  uppercased = (function() {
    var _i, _len, _results;
    _results = [];
    for (_i = 0, _len = collection.length; _i < _len; _i++) {
      item = collection[_i];
      _results.push(item.toUpperCase());
    }
    return _results;
  })();

  cosole.log(uppercased);

}).call(this);

//# sourceMappingURL=appCoffee.js.map
