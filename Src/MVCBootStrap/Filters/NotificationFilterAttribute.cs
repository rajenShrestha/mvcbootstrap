﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 4/7/2015 8:43:42 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppMVCBootStrap.Models;

namespace WebAppMVCBootStrap.Filters
{
    public class NotificationFilterAttribute : ActionFilterAttribute
    {
        [OutputCache(Duration=60)]
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var context = new SiteDataContext();

            var notifications = context.Notifications
                .GroupBy(n => n.NotificationType)
                .Select(g => new NotificationViewModel
                {
                    Count = g.Count(),
                    NotificationType = g.Key.ToString(),
                    BadgeClass = NotificationType.Email == g.Key ? "success" : "info"
                });

            filterContext.Controller.ViewBag.Notifications = notifications; 
        }
    }
}