﻿using System.Web;
using System.Web.Mvc;
using WebAppMVCBootStrap.Filters;

namespace WebAppMVCBootStrap
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //Global registration
            //will execute on every request
            //filters.Add(new NotificationFilterAttribute());

           // filters.Add(new RequireHttpsAttribute());
        }
    }
}
