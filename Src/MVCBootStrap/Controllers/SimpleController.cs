﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppMVCBootStrap.Models;

namespace WebAppMVCBootStrap.Controllers
{
    public class SimpleController : Controller
    {
        // GET: Simple
        public ActionResult Index()
        {
            var person = new Person
            {
                FirstName = "Rajen",
                LastName = "Shrestha",
                BirthDate = new DateTime(1980, 01, 02),
                LikesMusic = true,
                Skills = new List<String> {"C#", "WPF", "ASP.NET", "MATHS" },
                EmailAddress = "rajenshrestha@hotmail.com"
            };
            return View(person);
        }

        public ActionResult Create()
        {
            var person = new Person();
            return View(person);
        }
    }
}