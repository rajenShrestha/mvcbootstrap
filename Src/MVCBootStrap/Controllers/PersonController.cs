﻿using Angela.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppMVCBootStrap.Filters;
using WebAppMVCBootStrap.Models;

namespace WebAppMVCBootStrap.Controllers
{
    public class PersonController : BaseController
    {

        private static ICollection<Person> _people;
        static PersonController()
        {
            //realistic test data
            //Thanks James : Hint: Install-Package AngelaSmith -Version 1.0.1
            _people = Angie.Configure<Person>()
                .Fill(p => p.BirthDate)
                .AsPastDate()
                .Fill(p => p.LikesMusic)
                .WithRandom(new List<bool>() { true, true, true, false, false })
                .Fill(p => p.Skills, () => new List<string>() { "Math", "Science", "History" })
                .MakeList<Person>(20);

        }

        public PersonController()
        {

        }

        [NotificationFilter]
        // GET: Person
        public ActionResult Index()
        {          

            return View(_people);
        }

        [HttpGet]
        public ActionResult SearchPeople(string searchText)
        {
            var term = Server.HtmlEncode(searchText.ToLower());
            var results = _people.Where(p => p.FirstName.ToLower().Contains(searchText) || p.LastName.ToLower().Contains(searchText));

            return PartialView("_SearchPeople", results);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var person = new Person();
            return View(person);
        }

        [HttpPost]
        public ActionResult Create(Person person)
        {
            if (ModelState.IsValid)
            {
                _people.Add(person);
                Success(string.Format("<b>{0}</b> was successfully added to the database", person.FirstName), true);
                return RedirectToAction("Index");
            }

            Danger("Looks like something went wrong. Please check your form.");
            return View(person);
        }
    }
}