namespace WebAppMVCBootStrap.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebAppMVCBootStrap.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WebAppMVCBootStrap.Models.SiteDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WebAppMVCBootStrap.Models.SiteDataContext context)
        {
            context.Notifications.AddOrUpdate(notification => notification.Title,
        new Notification
        {
            Title = "John Smith was added to the system.",
            NotificationType = NotificationType.Registration
        },
        new Notification
        {
            Title = "Susan Peters was added to the system.",
            NotificationType = NotificationType.Registration
        },
        new Notification
        {
            Title = "Just an FYI on Thursday's meeting",
            NotificationType = NotificationType.Email
        });
        }
    }
}
