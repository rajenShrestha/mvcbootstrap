﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 4/7/2015 6:44:51 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppMVCBootStrap.Models
{
    public enum NotificationType { Registration, Email}

    public class Notification
    {
        public int NotificationId { get; set; }
        public string Title { get; set; }
        public NotificationType NotificationType { get; set; }
        public string  Controller { get; set; }
        public string Action { get; set; }
    }
}