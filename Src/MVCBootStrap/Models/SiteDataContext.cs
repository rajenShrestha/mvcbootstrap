﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 4/7/2015 6:47:27 PM
*/
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebAppMVCBootStrap.Models
{
    public class SiteDataContext: DbContext
    {
        public SiteDataContext(): base("DefaultConnection")
        {

        }

        //DbSet represent a table in database
        public DbSet<Notification> Notifications { get; set; }
    }
}