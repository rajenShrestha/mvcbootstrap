﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 4/7/2015 7:33:21 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppMVCBootStrap.Models
{
    public class NotificationViewModel
    {
        public int Count { get; set; }
        public string NotificationType { get; set; }
        public string BadgeClass { get; set; }
    }
}